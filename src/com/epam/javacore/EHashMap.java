package com.epam.javacore;

import java.io.Serializable;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

public class EHashMap<K, V> extends AbstractMap<K, V> implements Map<K, V>, Cloneable, Serializable {

	private static final long serialVersionUID = -9008459708420744234L;

	transient Node<K, V>[] table;
	transient Set<Map.Entry<K, V>> entries;
	transient int size;

	static final float DEFAULT_LOAD_FACTOR = 0.75f;
	static final int MAXIMUM_CAPACITY = Integer.MAX_VALUE / 2 + 1;
	static final int DEFAULT_INITIAL_CAPACITY = 16;

	final float loadFactor;
	int threshold;

	public EHashMap() {
		this.loadFactor = DEFAULT_LOAD_FACTOR;
	}

	public EHashMap(int initialCapacity) {
		this(initialCapacity, DEFAULT_LOAD_FACTOR);
	}

	public EHashMap(int initialCapacity, float loadFactor) {
		if (initialCapacity < 0)
			throw new IllegalArgumentException("Illegal initial capacity: " + initialCapacity);
		if (initialCapacity > MAXIMUM_CAPACITY)
			initialCapacity = MAXIMUM_CAPACITY;
		if (loadFactor <= 0 || Float.isNaN(loadFactor))
			throw new IllegalArgumentException("Illegal load factor: " + loadFactor);
		this.loadFactor = loadFactor;
		this.threshold = tableSizeFor(initialCapacity);
	}

	public int size() {
		return size;
	}

	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public V put(K key, V value) {
		Node<K, V>[] tab;
		V prev = null;
		int n, i;
		if ((tab = table) == null || (n = tab.length) == 0) {
			n = (tab = resize()).length;
		}
		if ((tab[i = (n - 1) & hash(key)]) == null) {
			table[i] = new Node<K, V>(key, value, null);
		} else {
			Node<K, V> list = tab[(n - 1) & hash(key)];
			boolean done = false;
			do {
				if (list.getKey().equals(key)) {
					prev = list.getValue();
					list.setValue(value);
					done = true;
					break;
				}
				if (list.next != null) {
					list = list.next;
				}
			} while (list.next != null);
			if (!done) {
				list.next = new Node<K, V>(key, value, null);
			}
		}
		if (++size > threshold) {
			resize();
		}
		return prev;
	}

	@Override
	public V get(Object key) {
		Node<K, V>[] tab;
		Node<K, V> list;
		int n;
		if ((tab = table) != null && (n = tab.length) > 0 && (list = tab[(n - 1) & hash(key)]) != null) {
			while (list != null) {
				if (list.getKey().equals(key))
					return list.getValue();
				list = list.next;
			}
		}
		return null;
	}

	@Override
	public V remove(Object key) {
		Node<K, V>[] tab;
		Node<K, V> current;
		Node<K, V> prev = null;
		V value = null;
		int n;
		if ((tab = table) != null && (n = tab.length) > 0 && (current = tab[(n - 1) & hash(key)]) != null) {
			do {
				if (current.getKey().equals(key)) {
					if (prev == null) {
						value = current.getValue();
						current = current.getNext();
					} else {
						value = current.getValue();
						prev.next = current.getNext();
						current = prev;
					}
					break;
				}
				prev = current;
				current = current.next;
			} while (current != null);
			tab[(n - 1) & hash(key)] = current;
		}
		return value;
	}

	@SuppressWarnings("unchecked")
	final Node<K, V>[] resize() {
		Node<K, V>[] oldTab = table;
		int oldCap = (oldTab == null) ? 0 : oldTab.length;
		int oldThr = threshold;
		int newCap = 0, newThr = 0;
		if (oldCap > 0) {
			if (oldCap >= MAXIMUM_CAPACITY) {
				threshold = Integer.MAX_VALUE;
				return oldTab;
			} else if ((newCap = oldCap * 2) < MAXIMUM_CAPACITY && oldCap >= DEFAULT_INITIAL_CAPACITY)
				newThr = oldThr * 2;
		} else if (oldThr > 0) {
			newCap = oldThr;
		} else {
			newCap = DEFAULT_INITIAL_CAPACITY;
			newThr = (int) (DEFAULT_LOAD_FACTOR * DEFAULT_INITIAL_CAPACITY);
		}
		if (newThr == 0) {
			float ft = (float) newCap * loadFactor;
			newThr = (newCap < MAXIMUM_CAPACITY && ft < (float) MAXIMUM_CAPACITY ? (int) ft : Integer.MAX_VALUE);
		}
		threshold = newThr;
		Node<K, V>[] newTab = (Node<K, V>[]) new Node[newCap];
		table = newTab;
		if (oldTab != null) {
			for (int i = 0; i < oldCap; ++i) {
				Node<K, V> e;
				if ((e = oldTab[i]) != null) {
					oldTab[i] = null;
					newTab[hash(e.getKey()) & (newCap - 1)] = e;
				}
			}
		}
		return newTab;
	}

	static class Node<K, V> implements Map.Entry<K, V> {
		K key;
		V value;
		Node<K, V> next;

		public Node(K key, V value, Node<K, V> next) {
			this.key = key;
			this.value = value;
			this.next = next;
		}

		@Override
		public K getKey() {
			return key;
		}

		@Override
		public V getValue() {
			return value;
		}

		@Override
		public V setValue(V newValue) {
			V oldValue = value;
			value = newValue;
			return oldValue;
		}

		public Node<K, V> getNext() {
			return next;
		}

		public void setNext(Node<K, V> next) {
			this.next = next;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((key == null) ? 0 : key.hashCode());
			result = prime * result + ((value == null) ? 0 : value.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Node<?, ?> other = (Node<?, ?>) obj;
			if (key == null) {
				if (other.key != null)
					return false;
			} else if (!key.equals(other.key))
				return false;
			if (value == null) {
				if (other.value != null)
					return false;
			} else if (!value.equals(other.value))
				return false;
			return true;
		}

		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("Node [key=").append(key).append(", value=").append(value).append(", next=").append(next)
					.append("]");
			return builder.toString();
		}

	}

	static final int tableSizeFor(int cap) {
		int n = cap;
		n = n * 3 / 2 + 1;
		return (int) ((n < 0) ? 1 : (n >= MAXIMUM_CAPACITY) ? MAXIMUM_CAPACITY : n * DEFAULT_LOAD_FACTOR);
	}

	final int hash(Object key) {
		int h = key.hashCode();
		return (key == null) ? 0 : (h) ^ (h >>> 16);
		// return (key == null) ? 0 : 7;
	}

	@Override
	public Set<java.util.Map.Entry<K, V>> entrySet() {
		Set<Map.Entry<K, V>> es;
		return (es = entries) == null ? (entries = new EntrySet()) : es;
	}

	final class EntrySet extends AbstractSet<Map.Entry<K, V>> {

		@Override
		public Iterator<Map.Entry<K, V>> iterator() {
			return new EntryIterator();
		}

		@Override
		public int size() {
			return size;
		}
	}

	abstract class EHashIterator {
		Node<K, V> next;
		Node<K, V> current;
		int expectedModCount;
		int index;

		public EHashIterator() {
			Node<K, V>[] t = table;
			current = next = null;
			index = 0;
			if (t != null && size() > 0) {
				while (index < t.length) {
					next = t[index++];
					if (next != null) {
						break;
					}
				}
			}
		}

		public final boolean hasNext() {
			return next != null;
		}

		final Node<K, V> nextNode() {
			Node<K, V>[] t;
			Node<K, V> e = next;
			if (e == null) {
				throw new NoSuchElementException();
			}
			if ((next = (current = e).next) == null && (t = table) != null) {
				while (index < t.length) {
					next = t[index++];
					if (next != null) {
						break;
					}
				}
			}
			return e;
		}
	}

	final class EntryIterator extends EHashIterator implements Iterator<Map.Entry<K, V>> {
		public final Map.Entry<K, V> next() {
			return nextNode();
		}
	}

	@Override
	public String toString() {
		if (table.length == 0) {
			return "{}";
		}
		StringBuilder sb = new StringBuilder();
		sb.append('{');
		for (int i = 0; i < table.length; i++) {
			if (table[i] != null) {
				Node<K, V> node = table[i];
				sb.append("Bucket " + i + ": " + node.toString());
			}
		}
		return sb.append('}').toString();
	}

	public void printEHashMap() {
		for (Entry<K, V> entry : entrySet()) {
			System.out.println("key - " + entry.getKey() + ", value - " + entry.getValue());
		}
	}

	public static void main(String[] args) {
		EHashMap<String, String> eHashMap = new EHashMap<>();
		int count = 16;
		for (int i = 0; i < count; i++) {
			eHashMap.put("keyTest" + i, "valueTest" + i);
		}
		System.out.println("EHashMap: " + eHashMap);
		System.out.println("====================");
		System.out.println("Remove: " + eHashMap.remove("keyTest1"));
		System.out.println("====================");
		System.out.println("EHashMap: " + eHashMap);
		for (Entry<String, String> node : eHashMap.entrySet()) {
			System.out.println("key=" + node.getKey() + " value=" + node.getValue());
		}
	}

}
